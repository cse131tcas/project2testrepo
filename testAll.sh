#!/bin/bash

red="\033[1;31m"
green="\033[1;32m"
blue="\033[1;34m"
clear="\033[0m"

new="[ ${blue}NEW ${clear} ]"
pass="[ ${green}PASS${clear} ]"
fail="[ ${red}FAIL${clear} ]"

cd `dirname $0`

if [[ -x /software/common/gnu/bin/gdiff ]]; then
    differ=/software/common/gnu/bin/gdiff
else
    differ=diff
fi

if [[ -n $1 ]]; then
    tests=$(find $1 -name '*.rc')
else
    tests=$(find * -mindepth 1 -name '*.rc')
fi

for f in $tests; do
    my="`dirname $f`/`basename $f .rc`.my.out"
    ans="`dirname $f`/`basename $f .rc`.ans.out"
    err=$(../RC.sh $f 3>&1 1>$my 2>&3)
    dos2unix $my &> /dev/null
    if [[ -e $ans ]]; then
        diff=$($differ -u $ans $my)
        if [[ -z $diff ]]; then
            msg=$pass
	else
	    msg=$fail
	fi
    else
	mv $my $ans
	diff=$(<$ans)
	msg=$new
    fi
    echo -en $msg
    echo " $f"
    if [[ -n $err ]]; then echo "$err"; fi
    if [[ -n $diff ]]; then echo "$diff"; fi
done
