#!/bin/bash                                                                     
cd `dirname $0`

echo "Attempting to update SOtest"

which hg &> /dev/null
if [[ `echo $?` == 0 ]]; then
    echo "Using Mercurial"
    hg pull
    hg update
else
    echo "Downloading Tarball"
    wget http://bitbucket.org/elliottslaughter/cse131-sotest/get/tip.tar.gz
    tar xfz tip.tar.gz
    cp -r cse131-sotest/* .
    rm -rf cse131-sotest tip.tar.gz
fi
